﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class Selector : MonoBehaviour
{
    public Text score;
    public UILineRenderer line;

    private const string scoreKey = "score";
    
    private List<Cell> selectCells = new List<Cell>();
    private GameField gameField;
    private int collectDots;
    private float offsetHight;
    private float offsetWidth;
    public void Init(GameField field,float h,float w)
    {
        gameField = field;
        offsetHight = h / 2;
        offsetWidth = w / 2;

        collectDots = PlayerPrefs.GetInt(scoreKey, 0);
        score.text = collectDots.ToString();
    }

    public void Push(Cell cell)
    {
        if (selectCells.Count == 0)
        {
            selectCells.Add(cell);
            return;
        }

        if (IsCanConnect(selectCells[selectCells.Count-1], cell))
        {
            if (selectCells[0].id == cell.id && selectCells.Count>3)                                                            //проверка на зацикливание выбора точек
            {
                // selectCells.Add(cell);
                Reset();
                return;
            }

            if (selectCells.Count >= 2)
            {
                if (selectCells[selectCells.Count - 2].id == cell.id)
                {
                    selectCells.RemoveAt(selectCells.Count-1);
                    return;
                }
            }
            selectCells.Add(cell);
        }
    }

    public void Reset()
    {
        if (selectCells.Count>=2)
        {
            collectDots += selectCells.Count;
            foreach (var cell in selectCells)
            {
                cell.DeleteDot();
            }

            foreach (var cell in selectCells)
            {
                gameField.DropDot(cell);
            }
        }
        
        PlayerPrefs.SetInt(scoreKey,collectDots);
        score.text = collectDots.ToString();
        selectCells.Clear();
    }

    public void Update()
    {
        if (selectCells.Count > 1)
        {
            line.enabled = true;
            line.color = selectCells[0].GetDot().GetColor().color;
            List<Vector2> points = new List<Vector2>();
            for (int i = 0; i < selectCells.Count; i++)
            {
                points.Add(new Vector2(selectCells[i].centre.x-offsetWidth,selectCells[i].centre.y-offsetHight));;
            }
        
            line.Points = points.ToArray();
        }
        else
        {
            line.Points = null;
            line.enabled = false;
        }
    }

    private bool IsCanConnect(Cell from, Cell to)
    {
        if (from.id == to.id)
            return false;
        
        bool left = from.indexI - 1 == to.indexI && from.indexJ==to.indexJ && IsColorSame(from, to); 
        bool right = from.indexI + 1 == to.indexI && from.indexJ==to.indexJ && IsColorSame(from,to);
        bool up = from.indexJ + 1 == to.indexJ && from.indexI==to.indexI && IsColorSame(from, to); 
        bool down = from.indexJ - 1 == to.indexJ && from.indexI==to.indexI  && IsColorSame(from, to);
        
        bool horizontal = left || right;
        bool vertical = up || down;
        bool total = horizontal != vertical;                                                                            //чтоб не получалось диагоналей 

        return total;
    }

    private bool IsColorSame(Cell from, Cell to)
    {
        return from.GetDot().GetColor().type == to.GetDot().GetColor().type;
    }
    
    private void OnDrawGizmos()
    {
        if (selectCells.Count >= 2)
        {
            Gizmos.color = Color.blue;
            for (int i = 0; i < selectCells.Count-1; i++)
            {
                  Gizmos.DrawLine(selectCells.ToArray()[i].centre,selectCells.ToArray()[i+1].centre);
            }
        }
    }
}
