﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DotColor
{
    public Color color;

    public GameSettings.ColorType type;
}
