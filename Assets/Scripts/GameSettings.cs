﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public static GameSettings instance;

    public Dot DotPrefab;
    public DotColor[] colors;

    private static int cellID = 0;
    void Awake()
    {
        instance = this;
    }

    public int GetCellID()
    {
        cellID++;
        return cellID;
    }

    public DotColor GetRandomColor()
    {
        return colors[Random.Range(0, colors.Length)];
    }

    public Dot InstantiateDot(Transform form)
    {
        Dot d = Instantiate(DotPrefab, form);
        return d;
    }
    
    public enum ColorType
    {
        Color1,
        Color2,
        Color3,
        Color4,
        Color5
    }
}
