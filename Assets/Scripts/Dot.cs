﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dot:MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private RectTransform rect;
    private DotColor dotColor;
    private Vector2 newPos;
    private Vector2 currentPos;
    private float time = 0;
    private bool isMove = false;

    public void InitDot(Vector2 pos,DotColor color)
    {
        transform.position = pos;
        currentPos = pos;
        dotColor = color;
        image.color = dotColor.color;
    }

    public void Move(Vector2 pos)
    {
        newPos = pos;
        isMove = true;
    }

    public DotColor GetColor()
    {
        return dotColor;
    }

    private void Update()
    {
        if(!isMove)
            return;

        time += Time.deltaTime;
        transform.position = Vector2.Lerp(currentPos, newPos, time);
        if (time > 1)
        {
            currentPos = newPos;
            time = 0;
            isMove = false;
        }
    }
}
