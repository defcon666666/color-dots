﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameField : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private RectTransform background;
    [SerializeField] private Selector selector;
    private RectTransform fieldRect;
    
    private Cell[,] grid;
    private Cell firstCell;
    private Cell lastCell;
    private float offsetX = 100;
    private int gridSize = 10;
    private int dropIndex = -1;
    private bool isInit = false;
    
    private void Start()
    {
        
        float x = background.rect.width - offsetX * 2;

        fieldRect = GetComponent<RectTransform>();
        fieldRect.sizeDelta = new Vector2(x,x);
        fieldRect.anchoredPosition = Vector2.zero;
        
        CreateGrid(gridSize);
        
        selector.Init(this,mainCamera.pixelHeight,mainCamera.pixelWidth);

        isInit = true;
    }

    private void CreateGrid(int size)
    {
        grid = new Cell[size,size];
        
        Vector3 worldPos = fieldRect.transform.TransformPoint(fieldRect.rect.position);
        
        float h = fieldRect.rect.height / size;
        float w = h;

        for (int i = 0; i < grid.GetLength(0); i++)
        {
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                Vector2 celPos = new Vector2(worldPos.x+i*h,worldPos.y+j*w);
                
                grid[i,j] = new Cell(celPos,h,w,i,j);
                grid[i,j].CreateDot(GameSettings.instance.InstantiateDot(transform),GameSettings.instance.GetRandomColor());
            }
        }

        firstCell = grid[0, 0];
        lastCell = grid[grid.GetLength(0)-1, grid.GetLength(1)-1];
    }
    

    // Update is called once per frame
    private void Update()
    {
        if(!isInit)
            return;
        
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Input.mousePosition;

            InsideGrid(mousePos);
        }

        if (Input.GetMouseButtonUp(0))
        {
            selector.Reset();
        }
    }

    private void InsideGrid(Vector2 mousePos)
    {
        if(mousePos.x > firstCell.position.x && mousePos.x<lastCell.size.x)                //попадает ли мышь на игровое поле
            if (mousePos.y > firstCell.position.y && mousePos.y < lastCell.size.y)
            {
                FindCell(mousePos,grid);
            }
    }

    private void FindCell(Vector2 mousePos, Cell[,] grid)
    {
        for (int i = 0; i < grid.GetLength(0); i++)
        {
            if(mousePos.x>grid[i,i].size.x)                                                 //сразу отсекаем весь столбец 
                continue;
            
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                if (grid[i, j].Inside(mousePos))
                {
                    selector.Push(grid[i,j]);
                    break;
                }
            }
        }
    }

    public void DropDot(Cell emptyCell)
    {
        if(emptyCell.indexI==dropIndex)
            return;
        
        dropIndex = emptyCell.indexI;

        Fall(dropIndex);
    }

    private void Fall(int i)
    {
        int emptyCell=0;
        
        if(grid[i,grid.GetLength(1)-1].GetDot()==null)
            grid[i,grid.GetLength(1)-1].CreateDot(GameSettings.instance.InstantiateDot(transform),GameSettings.instance.GetRandomColor());
        
        for (int j = grid.GetLength(1)-2; j>=0 ; j--)
        {
            if (grid[i, j].GetDot() == null)
            {
                grid[i,j].ReplaceDot(grid[i,j+1]);
            }
        }

        for (int j = 0; j < grid.GetLength(1) - 1; j++)
        {
            if (grid[i, j].GetDot() == null)
            {
                emptyCell++;
            }
        }
        
        if(emptyCell==0)
            return;
        
        Fall(i);
    }
}
