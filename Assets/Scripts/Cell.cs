﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    
    public Vector2 position;
    public Vector2 size { get; }
    public Vector2 centre { get; }
    
    public int id { get; }
    public int indexI { get; }
    public int indexJ { get; }
    public float width { get; }
    public float hight{ get; }
    
    private Dot dot;

    public Cell(Vector2 pos, float h, float w,int i,int j)
    {
        position = pos;
        hight = h;
        width = w;
        indexI = i;
        indexJ = j;
        
        size = new Vector2(position.x+width,position.y+hight);
        centre =new Vector3((position.x+size.x)/2,(position.y+size.y)/2);
        id = GameSettings.instance.GetCellID();
        
        Debug.DrawLine(position,position+Vector2.up*hight,Color.green,1000f);
        Debug.DrawLine(position,position+Vector2.right*width,Color.green,1000f);
    }

    public void CreateDot(Dot d,DotColor c)
    {
        dot = d;
        dot.InitDot(centre,c);
    }

    public void ReplaceDot(Cell dotOwner)
    {
        dot = dotOwner.GetDot();
        dotOwner.RemoveDot();
        dot.Move(centre);
    }

    public void RemoveDot()
    {
        dot = null;
    }

    public void DeleteDot()
    {
        Destroy(dot.gameObject);
        dot = null;
        
    }

    public Dot GetDot()
    {
        return dot;
    }

    public bool Inside(Vector2 mousePos)
    {
        bool x = mousePos.x >= position.x && mousePos.x <= position.x + width;
        bool y = mousePos.y >= position.y && mousePos.y <= position.y + hight;

        if (x && y)
        {
            Debug.DrawLine(position,size,Color.red,0.3f);
        }

        return x && y;
    }
}
